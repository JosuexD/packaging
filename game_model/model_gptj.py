from transformers import GPTNeoForCausalLM, GPT2Tokenizer, StoppingCriteria, StoppingCriteriaList, MaxTimeCriteria
import time

model = GPTNeoForCausalLM.from_pretrained("EleutherAI/gpt-neo-1.3B")
tokenizer = GPT2Tokenizer.from_pretrained("EleutherAI/gpt-neo-1.3B")

history = []
user = "fan:"

def add_prompt(prompt):
    print("Adding prompt to the model")

    # Test Prompts


    # prompt = (
    #     "In a shocking finding, scientists discovered a herd of unicorns living in a remote, "
    #     "previously unexplored valley, in the Andes Mountains. Even more surprising to the "
    #     "researchers was the fact that the unicorns spoke perfect English."
    # )
    # prompt = ("This is Emma Watson answering questions to an interviewer, "
    # "Emma: so excited to be here!", 
    # "josue: not as excited as we are to interview you")
    prompt = """the following is a conversation between jack sparrow and one of his fans:
fan: omg it's jack sparrow in person!
jack: hello there matey, would you like an autograph?
fan: yes please!
jack:"""

    stop_prompt = ":"
    stop_ids = tokenizer(stop_prompt, return_sensors="pt").input_ids



    input_ids = tokenizer(prompt, return_tensors="pt").input_ids

    # Adding stop Criterias to the model
    st = StoppingCriteriaList(stop_ids)
    t_criteria = MaxTimeCriteria(max_time = 15.0)
    criteria_list = StoppingCriteriaList([t_criteria])


    start = time.time()
    gen_tokens = model.generate(
        inputs = input_ids,
        do_sample=True,
        temperature=0.9,
        # max_length=len(prompt) + 10,
        max_length = 500,
        stopping_criteria = criteria_list,
        forced_eos_token_id = stop_ids,
        eos_token_id = stop_ids[0]
    )

    end = time.time()
    print(f"Inference Time: {end - start}")
    gen_text = tokenizer.batch_decode(gen_tokens)[0]

    print(gen_text)
    global history
    history = gen_text
    return gen_text


def parse_input(input):
    # TODO
    # Write specific language for this specific model. Whether that is setting some kind of specific prompt settings like some models require or somethign elser

    # Check if the input is asking f
    if "COMMAND_PROMPT" in input:
        add_prompt(input)
    else:
        generate_respose(input)

def generate_respose(input):
    global history

    # construct the input the way it is intended
    question = f"\n{user} {input}"
    updated_input = history + question + f"\njack:"
    input_ids = tokenizer(updated_input, return_tensors="pt").input_ids
    gen_tokens = model.generate(
        inputs = input_ids,
        do_sample=True,
        temperature=0.9,
        max_length=50,
    )
    gen_text = tokenizer.batch_decode(gen_tokens)[0]

    # Updating history again
    history = gen_text
    return gen_text



if __name__ == "__main__":
    print(add_prompt("something"))

    # while True:
    #     user_input = input("Insert questions for AI:\n")
    #     print(generate_respose(user_input))
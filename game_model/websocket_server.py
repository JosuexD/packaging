import asyncio

import websockets
import audio_dnn_processor
# import fideo_model
import model_gptj

URL = "localhost"
PORT = 8000
# create handler for each connection
async def handler(websocket, path):
    while True:
        data = await websocket.recv()
        # Process Message through NLP and receive response
       # generated_response = audio_dnn_processor.generate_response(data, False)
        # Forward response to unreal server
        # generated_response = fideo_model.generate_response(data)
        generated_response = model_gptj.parse_input(data)
        await websocket.send(generated_response)
        # Play audio
        audio_dnn_processor.SpeakText(generated_response)


if __name__ == "__main__":
    start_server = websockets.serve(handler, URL, PORT)

    print(f"Starting the server on {URL}:{PORT}")
    print("Press ctrl+c to close server...")
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
